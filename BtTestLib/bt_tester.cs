﻿using System;

using Windows.Storage.Streams;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;

namespace BtTestLib
{
    public class BtTester
    {
        byte[] heartData;
        String[] deviceList;
        GattCharacteristic data;
        GattDeviceService service;
        int worked = 0;

        public BtTester()
        {
        }

        public async void getDevice(String deviceName)
        {
            var devices = await DeviceInformation.FindAllAsync(GattDeviceService.GetDeviceSelectorFromUuid(GattServiceUuids.HeartRate), 
                new string[] { "System.Devices.ContainerId" });
            deviceList = new String[devices.Count];

            for (int i = 0; i < devices.Count; ++i)
            {
                deviceList[i] = devices[i].Name;
                if (devices[i].Name == deviceName)
                {
                    service = await GattDeviceService.FromIdAsync(devices[i].Id);
                    data = service.GetCharacteristics(GattCharacteristicUuids.HeartRateMeasurement)[0];
                    data.ValueChanged += data_ValueChanged;
                }
            }
        }

        void data_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            worked = 1;
            var newData = new byte[args.CharacteristicValue.Length];

            DataReader.FromBuffer(args.CharacteristicValue).ReadBytes(newData);
            heartData = newData;
        }

        public String[] getDeviceList()
        {
            return deviceList;
        }

        public byte[] getData()
        {
            return heartData;
        }

        public int getWorked()
        {
            return worked;
        }
    }
}
